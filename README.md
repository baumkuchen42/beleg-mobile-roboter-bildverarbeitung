# necessary to run the program
- python3
- gtk, ideally gtk3 
- pip3
- python-opencv --> `pip3 install opencv-python`
- python module aenum --> `pip3 install aenum`
- python module imutils --> `pip3 install imutils`

# how to run
- clone project
- navigate to `src/`
- `python3 main.py`

# possible future features
- Elfmetermodus
- log events with timestamp
- write tests

# documentation
- navigate to `docs/build/html/`
- open `index.html` to get to documentation start page