# necessary to run the program:
- python3
- gtk, ideally gtk3 
- pip3
- python-opencv --> `pip3 install opencv-python`
- python module aenum --> `pip3 install aenum`
- python module imutils --> `pip3 install imutils`

# possible future features
- Elfmetermodus
- log events with timestamp
- write tests
