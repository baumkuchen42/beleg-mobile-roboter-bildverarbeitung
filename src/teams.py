class Team():
	"""
	Class to create the two team instances with score and name.
	"""
	def __init__(self, name):
		"""
		Creates new team with initial score of 0.

		:param name: name of the team
		:type name: in this case either "Team1" or "Team2"
		"""
		self.__name = name
		self.__score = 0

	@property
	def score(self):
		"""
		Property to access value of ``score`` with read rights.

		:returns: score of the team object
		:rtype: number
		"""
		return self.__score

	@score.setter
	def score(self, new_score):
		"""
		Property to access value of ``score`` with write rights.

		:param new_score: the new score to be set for the team
		:type new_score: should be number
		"""
		self.__score = new_score


	@property
	def name(self):
		"""
		Property to access value of ``name`` with read rights.

		:returns: name of the team object
		:rtype: no strict type, but most likely string
		"""
		return self.__name
