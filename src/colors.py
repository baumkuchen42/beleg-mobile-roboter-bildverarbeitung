from aenum import Enum #use aenum to have the possibility of more than one attribute per enum instance

class Color(Enum):
	"""
	Defines possible colors (for color tracking)
	and their upper and lower threshholds.
	"""
	def __init__(self, thresh_lower, thresh_upper):
		"""
		constructor for enum from type colors

		:param thresh_lower: low hsv border where it's still the color
		:type thresh_lower: tuple
		:param thresh_upper: high hsv border where it's still the color
		:type thresh_upper: tuple
		"""
		self.thresh_lower = thresh_lower
		self.thresh_upper = thresh_upper

	RED = (0, 215, 69), (14, 255, 255)
	GREEN = (40, 162, 75), (126, 241, 255)
	BLUE = (45, 148, 82), (119, 184, 253)
	YELLOW = (0, 239, 225), (241, 255, 255)
