import cv2
import imutils
import time
from capture_frame import *
from colors import Color

def track_color(frame, color : Color, show_ball=False):
	"""
		Tracks given color and optionally shows the frame with circled object.

		:param frame: the frame where to search object with given color
		:type frame: matrix
		:param color: which color to track
		:type color: enum from ``colors.py``
		:return: center coordinates of object with tracked color
		:rtype: tuple
	"""
	#blur the frame and convert it to the HSV color space
	blurred = cv2.GaussianBlur(frame, (11, 11), 0)
	hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

	#construct a mask for the color, then perform a series of dilations and erosions to
	#remove any small blobs left in the mask
	mask = cv2.inRange(hsv, color.thresh_lower, color.thresh_upper)
	mask = cv2.erode(mask, None, iterations=2)
	mask = cv2.dilate(mask, None, iterations=2)

	#find contours in the mask
	cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
	cnts = imutils.grab_contours(cnts)

	#only proceed if at least one contour was found
	if len(cnts) > 0:
		#find the largest contour in the mask, then use
		#it to compute the minimum enclosing circle and centroid
		c = max(cnts, key=cv2.contourArea)
		((x, y), radius) = cv2.minEnclosingCircle(c)
		M = cv2.moments(c)
		center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
		if show_ball is True:
			cv2.circle(frame, (int(x), int(y)), int(radius),(0, 255, 255), 2)
			cv2.circle(frame, center, 5, (0, 0, 255), -1)
			cv2.imshow('Tracking Color', frame)
		return center

if __name__=='__main__':
	"""
	executes default action of color_tracking.py
	which is tracking a red ball
	"""
	stream = cv2.VideoCapture('rtsp://141.46.137.93:8554/mystream')
	while stream.isOpened():
		frame = capture_and_crop(stream)
		print(track_color(frame, Color.RED, show_ball=True))
		if cv2.waitKey(1) == 27: #ESC
			break
	else:
		print('Error open video')

	stream.release()
	cv2.destroyAllWindows()
