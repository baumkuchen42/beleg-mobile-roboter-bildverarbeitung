import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib, Gdk

from teams import Team

class Gui:

	def __init__(self):
		"""
		Initalises a new builder object to import ui file and calls method to show window.
		"""
		self.builder = Gtk.Builder()
		self.builder.add_from_file('scoreboard.ui')
		self.show_window()

	def show_window(self):
		"""
		Imports window object, performs last construction steps and shows window.
		"""
		window = self.builder.get_object('score_window')
		window.set_title('Scoreboard')
		window.connect('destroy', Gtk.main_quit)
		window.show_all()

	def incr_score(self, team : Team):
		"""
		Increases the left or the right number, depending on which team scored a goal.

		:param team: from which team the score is to be increased
		:type team: instance of ``teams.py``
		"""
		score_label = NotImplemented
		if team.name is 'Team 1':
			score_label = self.builder.get_object('score_team1')
		elif team.name is 'Team 2':
			score_label = self.builder.get_object('score_team2')
		score_label.set_text(str(int(score_label.get_text()) +1)) #needs to be typecasted twice bc label texts are strings
																  #but incrementation only works with numbers

if __name__ == '__main__':
	Gui()
	Gtk.main() #starts its own gtk main thread if standalone script
