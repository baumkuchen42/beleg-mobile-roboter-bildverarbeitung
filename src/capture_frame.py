import cv2

def capture_and_crop(stream):
	"""Retrieves frame from given stream and provides it in cropped format.

	:param  stream: should be a cv2.VideoCapture from IP or block device like webcam
	:return: a from the stream extracted and cropped frame
	:rtype: matrix
	"""
	frame_width = stream.get(cv2.CAP_PROP_FRAME_WIDTH)
	frame_height = stream.get(cv2.CAP_PROP_FRAME_HEIGHT)
	ret, frame = stream.read()
	if not ret:
		print("Error retrieving video frame")
	if frame is not None: #the crop values are adjusted so that frame only shows playfield
		x1 = 350
		x2 = int(frame_width) - 400
		y1 = 200
		y2 = int(frame_height) - 200
		cropped_frame = frame[y1:y2, x1:x2]
		return cropped_frame


if __name__=='__main__':
	#If script is run as standalone it gets the stream itself.
	stream = cv2.VideoCapture('rtsp://141.46.137.93:8554/mystream')
	while(stream.isOpened()):
		cv2.imshow('Video', capture_and_crop(stream))
		if cv2.waitKey(1) == 27: #ESC
			break 
	else:
		print('Error open video')

	stream.release()
	cv2.destroyAllWindows()
