import cv2
from multiprocessing import Process
import time
import sys
import random

from capture_frame import *
from color_tracking import *
from colors import Color
from server import *
from teams import Team
from scoreboard_gui import Gui

class Main:
	def __init__(self):
		"""
		Initiates all necessary attributes and starts the stream.
		"""
		self.stream = cv2.VideoCapture('rtsp://141.46.137.93:8554/mystream')
		self.team1 = Team('Team 1')
		self.team2 = Team('Team 2')

		self.coordinates_red_ball = None
		self.coordinates_team1 = None
		self.coordinates_team2 = None

		self.gui = NotImplemented
		self.server = NotImplemented

		self.game_length = 5*60 #in seconds, currently 5 min.

		#goal corners to enclose whole goal area
		self.min_goal_corner_team1 = (1013, 250)
		self.max_goal_corner_team1 = (1146, 569)
		self.min_goal_corner_team2 = (117, 255)
		self.max_goal_corner_team2 = (146, 414)

		self.waiting_time = 0.1
		self.stagnation_tolerance = (1, 1) #may need to be adjusted after praxis testing

		#two different penalty zone x-coordinates to distinguish
		#between zone violation (border) and team's start position (within)
		self.left_penalty_zone_border = 840
		self.right_penalty_zone_border = 320
		self.left_penalty_zone_within = 910
		self.right_penalty_zone_within = 240

		#game field borders, used to determine when ball is out of game
		self.min_out_x = 151

		self.max_out_x = 1008
		self.min_out_y = 39
		self.max_out_y = 617

		time.sleep(1) #wait a bit for the stream before starting

	def start_server_and_initiate_game(self):
		"""
		Starts server, waits for clients and initiates game
		when at least two EV3s are connected.
		"""
		self.server = Server('141.46.137.83', 12345) #ip address of the device this program runs on
		Process(target=self.server.server_loop, daemon=True).start()
		print('waiting for clients to connect')
		#self.main() #for debugging reasons at this point (bc there are no clients yet to be awaited) 
		while self.server.get_nr_of_clients() < 2: #should be two clients (both teams)
			time.sleep(self.waiting_time)
		else:
			time.sleep(3)
			self.main()

	def main(self):
		"""
		Starts timer and scoreboard GUI, then main loop
		with game surveillance.
		"""
		#self.start_scoreboard_gui() #doesn't show, bug has low priority though
		first_loop = True
		while(self.stream.isOpened()):
			frame = capture_and_crop(self.stream)
			if frame is not None:
				self.track_ev3s_and_ball(frame)
				#if it's the first loop and ball and ev3s are found, the main logic is started
				if (first_loop 
				and self.coordinates_red_ball is not None
				and self.coordinates_team1 is not None
				and self.coordinates_team2 is not None):
					self.ev3_back_to_start_position() #before game can start, ev3s must be in start position
					Process(target=self.goal_surveillance, daemon=True).start()
					Process(target=self.penalty_zone_surveillance, daemon=True).start()
					Process(target=self.ball_out_of_game_surveillance, daemon=True).start()
					self.server.send_start_signal()
					Process(target=self.timer, args=(self.game_length,), daemon=True).start()
					time.sleep(5) #wait a bit before starting stagnation surveillance
					Process(target=self.stagnation_surveillance, daemon=True).start()
					first_loop = False
			if cv2.waitKey(1) == 27: #ESC
				break
		else:
			print('Error open video stream')
		self.close_program()


####### surveillance methods #############################################################

	def track_ev3s_and_ball(self, frame):
		"""
		Uses method ``track_color`` from module ``color_tracking.py`` to find ball
		and EV3 with yellow and EV3 with green team color
		and sets global variables accordingly.

		:param frame: current frame from stream where EV3s and ball should be found in
		:type frame: matrix
		"""
		self.coordinates_red_ball = track_color(frame, Color.RED, show_ball=False)
		self.coordinates_team1 = track_color(frame, Color.YELLOW)
		self.coordinates_team2 = track_color(frame, Color.GREEN)
		print(self.coordinates_red_ball, self.coordinates_team1, self.coordinates_team2) #debugging

	def goal_surveillance(self):
		"""
		Keeps track of goals and informs teams about them
		to initiate after-goal-routine.
		"""
		while True:
			try:
				if self.detect_goal(self.team1):
					print('GOAL TEAM 1, new score is', self.print_score())
					self.incr_goal_counter(self.team1)
					self.server.send_goal_signal(self.team1)
					self.ev3_back_to_start_position()
				elif self.detect_goal(self.team2):
					print('GOAL TEAM 2, new score is', self.print_score())
					self.incr_goal_counter(self.team2)
					self.server.send_goal_signal(self.team2)
					self.ev3_back_to_start_position()
			except TypeError: #occurs when of of the coordinates of ball or ev3s is None
				pass
			time.sleep(self.waiting_time)

	def detect_goal(self, team : Team):
		"""
		Checks if center of ball is in goal territory.

		:param team: specifies for which team to check if they scored  a goal
		:type team: instance of ``teams.py``
		:return: if team scored a goal
		:rtype: bool
		"""
		if team is self.team1:
			return self.min_goal_corner_team2 <= self.coordinates_red_ball <= self.max_goal_corner_team2
		elif team is self.team2:
			return self.min_goal_corner_team1 <= self.coordinates_red_ball <= self.max_goal_corner_team1

	def penalty_zone_surveillance(self):
		"""
			Detects zone violations, meaing that the EV3 of one team drove into
			the penalty zone in front of the other team's goal.
		"""
		while True:
			try:
				if self.in_penalty_zone(self.team1, own_zone=False):
					self.server.send_zone_violation_signal(self.team1)
					print('Zone violation Team 1, Ball goes to Team 2')
					self.ev3_back_to_start_position()
				if self.in_penalty_zone(self.team2, own_zone=False):
					self.server.send_zone_violation_signal(self.team2)
					print('Zone violation Team 2, Ball goes to Team 1')
					self.ev3_back_to_start_position()
			except TypeError: #occurs when of of the coordinates of ball or ev3s is None
				pass
			time.sleep(self.waiting_time)

	def in_penalty_zone(self, team : Team, own_zone : bool):
		"""
			Depending on whether it should be detected if the team's EV3
			is fully in its own zone (namely the start position) or whether
			it has touched the opponent's zone, different values are used to
			detect if in penalty zone.

			:param team: which team is to be checked
			:type team: instance of ``teams.py``
			:param own_zone: if it should be evaluated whether EV3 is back in own zone or in the opponent's zone (makes difference in exact place)
			:type own_zone: bool
			:return: whether EV3 of given team is touching opponent's zone or is within its own zone
			:rtype: bool
		"""
		if own_zone:
			if team is self.team1:
				return self.coordinates_team1[0] >= self.left_penalty_zone_within
			elif team is self.team2:
				return self.coordinates_team2[0] <= self.right_penalty_zone_within
		else:
			if team is self.team1:
				return self.coordinates_team1[0] <= self.right_penalty_zone_border
			elif team is self.team2:
				return self.coordinates_team2[0] >= self.left_penalty_zone_border

	def ball_out_of_game_surveillance(self):
		"""
		Monitors ball whether it gets out of game, if yes, sends signal.
		"""
		while True:
			try:
				if self.detect_out() and not self.detect_goal(self.team1) and not self.detect_goal(self.team2): #even though the goals are out of the playfield too,
																												#they don't count as out
					print('Ball is out of game')
					self.server.send_out_signal()
			except TypeError: #occurs when of of the coordinates of ball or ev3s is None
				pass

	def detect_out(self):
		"""
		Checks if the ball is out of the game,
		in x- or in y-direction.

		:return: if ball is out of game
		:rtype: bool
		"""
		return (self.coordinates_red_ball[0] <= self.min_out_x
				or self.coordinates_red_ball[0] >= self.max_out_x
				or self.coordinates_red_ball[1] <= self.min_out_y
				or self.coordinates_red_ball[1] >= self.max_out_y)

	def stagnation_surveillance(self):
		"""
			Detects stagnation, meaning none of the EV3s is moving anymore.
			``stagnation_tolerance`` defines the minimum change of position
			which the program still perceives as "real movement".
			If stagnation is detected, it sends signal and initiates
			back to start position routine.
		"""
		while True:
			try:
				starting_point1 = self.coordinates_team1
				starting_point2 = self.coordinates_team2
				time.sleep(5) #only reacts if ev3 are in deadlock for more than 5 secounds
				if (tuple(abs(i-j) for i,j in zip(self.coordinates_team1, starting_point1)) <= self.stagnation_tolerance and
					tuple(abs(i-j) for i,j in zip(self.coordinates_team2, starting_point2)) <= self.stagnation_tolerance):
						self.server.send_stagnation_signal()
						print('EV3s are not moving anymore, sending stagnation signal')
						self.ev3_back_to_start_position()
			except TypeError: #occurs when of of the coordinates of ball or ev3s is None
				pass
			time.sleep(self.waiting_time)


####### other game logic routines ###########################################################

	def ev3_back_to_start_position(self):
		"""Waits for the EV3s to return to their start position and singalises resume."""
		while (not self.in_penalty_zone(self.team1, own_zone=True)
			or not self.in_penalty_zone(self.team2, own_zone=True)):
				pass
		else:
			self.server.send_resume_signal()

	def order_free_kick(self):
		"""
		Team with lowest score gets the free kick, in case of draw random decision.
		Server sends signals accordingly.
		"""
		#might be superfluous
		if self.team1.score < self.team2.score:
			self.server.send_free_kick_signal(self.team1)
		elif self.team2.score < self.team1.score:
			self.server.send_free_kick_signal(self.team2)
		else:
			self.server.send_free_kick_signal(random.choice(list(self.team1, self.team2)))


######## keeping and announcing score #####################################

	def incr_goal_counter(self, team : Team):
		"""
		Increases the internal score count and shows it in the GUI, too.

		:param team: which team's score is to be increased
		:type team: instance of ``teams.py``
		"""
		team.score = team.score +1
		#self.gui.incr_score(team)

	def print_score(self):
		"""
		Pretty-prints the score, always team1 on the left
		and team2 on the right.
		"""
		print(self.team1.score, ':', self.team2.score)

	def announce_winner(self):
		"""
		Evaluates the winner or if it's a draw and prints final result.
		"""
		if self.team1.score > self.team2.score:
			print('WINNER: TEAM1')
		elif self.team2.score > self.team1.score:
			print('WINNER: TEAM2')
		else:
			print('DRAW')


####### other methods #########################################################

	def timer(self, duration):
		"""
		Measures game time and initiates closing process as soon as time runs out.

		:param duration: how long the timer should run
		:type duration: number
		"""
		print('timer started')
		time.sleep(duration)
		print('timer stopped')
		time.sleep(self.waiting_time)
		self.close_program()

	def start_scoreboard_gui(self):
		"""Initiates the scoreboard GUI as attribute ``self.gui``"""
		self.gui = Gui()

	def close_program(self):
		"""
			Signalises game ending to the teams,
			closes all open connections etc. and
			announces winner/draw.
		"""
		self.server.send_end_signal()
		self.server.close_all_sockets()
		cv2.destroyAllWindows()
		self.stream.release()
		self.announce_winner()
		sys.exit()

if __name__=='__main__':
	"""
		Creates a new object of class "Main" and calls initiating-method,
		program can be interrupted with ctrl+c.
	"""
	new_main = Main()
	try:
		new_main.start_server_and_initiate_game()
	except KeyboardInterrupt:
		new_main.close_program()
