import socket
import selectors
import types
import time

from teams import Team

class Server:
	def __init__(self, host_ip, port):
		"""
		Opens a new server socket.

		:param host_ip: to which address to bind socket
		:type host_ip: string
		:param port: which port to use for the socket
		:type port: integer
		"""
		self.timeout = 2 #default timeout, if no timeout needed, timeout is set to None
		self.default_sel = selectors.DefaultSelector() #default selector of server socket
		lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		#as long as OSError occurs it tries again to bind address
		connected = False
		while not connected:
			try:
				lsock.bind((host_ip, port))
				connected = True
			except OSError:
				pass
		lsock.listen()
		print('listening on', (host_ip, port))
		lsock.setblocking(False)
		self.default_sel.register(lsock, selectors.EVENT_READ, data=None)

	def server_loop(self):
		"""
		Waits for clients to connect and then listens to client signals.
		"""
		print('started server main loop')
		while True:
			events = self.default_sel.select(timeout=None)
			for key, mask in events:
				if key.data is None:
					self.accept_wrapper(key.fileobj)
				else:
					try:
						self.listen_to_client_signals(key, mask)
					except ConnectionResetError: #this error occurs when a client closes the connection itself
						print('One client closed its connection')
					except Exception as err:
						print(err)

	def accept_wrapper(self, sock):
		"""
		Accepts new client and defines data type and events for the connection.

		:param sock: the fileobject belonging to the key of the client who wants to connect to server
		"""
		conn, addr = sock.accept()
		print('accepted connection from', addr)
		conn.setblocking(False)
		data = types.SimpleNamespace(addr=addr, inb=b'', outb=b'')
		events = selectors.EVENT_READ | selectors.EVENT_WRITE
		self.default_sel.register(conn, events, data=data)

	def listen_to_client_signals(self, key, mask):
		"""
		Listens at the client connection for the cmd ``'A'``
		which is used for the client to cancel the connection.

		:param key: key of the client
		:param mask: mask of the client
		"""
		sock = key.fileobj
		data = key.data
		if mask & selectors.EVENT_READ: #if socket is ready to read
			recv_data = sock.recv(1024)  #1024 is the maximum amount of data to be received at once
			if recv_data is 'A': #client sends cmd to close connection
				print('closing connection to', data.addr)
				self.default_sel.unregister(sock)
				sock.close()

	def send_start_signal(self):
		"""
		Sends signal for starting the game.
		"""
		self.send_data('S')
		print('sent start signal')

	def send_end_signal(self):
		"""
		Sends signal for stopping the game.
		"""
		self.send_data('X')
		print('sent stop signal')

	def send_goal_signal(self, team : Team):
		"""
		Sends signal for goal, concrete signal depends on team which scored goal.

		:param team: team which scored goal
		:type team: instance of ``teams.py``
		"""
		if team.name is 'Team 1':
			self.send_data('T')
			print('sent team1 goal signal')
		elif team.name is 'Team 2':
			self.send_data('U')
			print('sent team2 goal signal')

	def send_free_kick_signal(self, team : Team):
		"""
		Sends signal for free kick,
		concrete signal depends on which team has free kick.
		This method is not used anymore as there is currently
		no free kick implemented in the game logic.

		:param team: team which has a free kick
		:type team: instance of ``teams.py``
		"""
		#not needed right now, left in for future uses
		if team.name is 'Team 1':
			self.send_data('F')
			print('sent team1 free kick signal')
		elif team.name is 'Team 2':
			self.send_data('G')
			print('sent team2 free kick signal')

	def send_stagnation_signal(self):
		"""
		Sends signal for deadlock/stagnation.
		"""
		self.send_data('F')
		print('sent deadlock/stagnation signal')

	def send_zone_violation_signal(self, team : Team):
		"""
		Sends signal for zone violation,
		concrete signal depends on which team violated the opponent's zone.

		:param team: team that violated opponent's zone
		:type team: instance of ``teams.py``
		"""
		if team.name is 'Team 1':
			self.send_data('H')
			print('sent team1 zone violation signal')
		elif team.name is 'Team 2':
			self.send_data('I')
			print('sent team2 zone violation signal')

	def send_out_signal(self):
		"""
		Sends signal for ball out of game.
		"""
		self.send_data('B')
		print('sent out signal')

	def send_resume_signal(self):
		"""
		Sends resume signal, used to continue game after having waited for some event.
		"""
		self.send_data('R')
		print('sent resume signal')

	def send_data(self, data):
		"""
		Loops through all client connections and sends them the data in binary format.

		:param data: letter which is to be send
		:type data: string (here with length 1)
		"""
		events = self.default_sel.select(timeout=self.timeout)
		for key, mask in events:
			if key.data is not None and mask & selectors.EVENT_WRITE:
				sock = key.fileobj
				try:
					sock.send(b'\x00' + data.encode()) #the binary zero (b'\x00) to fit the format of the java method readChar
				except BrokenPipeError: #this error occurs when the client disconnects in the process of sending something to it
					print('One client closed its connection')

	def get_nr_of_clients(self):
		"""
		Counts number of clients by determining length of connection list.

		:return: length of the list of selectors
		:rtype: integer
		"""
		return len(self.default_sel.select(timeout=self.timeout))

	def close_all_sockets(self):
		"""
		Loops through connection list and closes every one.
		"""
		events = self.default_sel.select(timeout=self.timeout)
		for key, mask in events:
			if key.data is not None:
				sock = key.fileobj
				sock.close()

if __name__=='__main__':
	new_server = Server('127.0.0.1', 12345)
	try:
		new_server.server_loop()
	except KeyboardInterrupt:
		new_server.close_all_sockets()
