src
===

.. toctree::
   :maxdepth: 4

   capture_frame
   color_tracking
   colors
   main
   scoreboard_gui
   server
   teams
