.. Roboterhockey documentation master file, created by
   sphinx-quickstart on Thu Jul  4 15:41:53 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Roboterhockey's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   capture_frame
   color_tracking
   colors
   main
   scoreboard_gui
   server
   teams


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
