# Regeln
- bevor das Spiel los geht, müssen beide EV3s auf ihren Startpositionen sein, erst dann wird Startsignal gesendet
- Deadlocksituationen am Ball verhindern (zu nah dran --> Roboter fahren nicht mehr weiter oder Verhaken)
- Tor erkennen (durch Koordinaten des Balls) & Kommando geben & Tor aufzeichnen, vermerkt, welches Tor
- nach Spielzeitende Stopsignal an alle EV3s vom Server gesendet (s.u.)

# Farben
- schwarz : Umrandung
- rot : Ball
- blau : Grundfarbe
- gelb : Team 1
- grün : Team 2

# Beleuchtung
- außen Lampen an (außer die Speziallampen der hinteren Tische), Lampen überm Tisch aus
- alternativ: auf die neuen Speziallampen warten

# Server
- Port: 12345
- IP: 141.46.137.83

## Protokoll für den Server
- 'S' : Startsignal
- 'X' : Spiel vorbei
- 'T' : Tor Team 1
- 'U' : Tor Team 2
- 'F' : Deadlock/Stagnation
- 'A' : clientseitiger Verbindungsabbruch
- 'H' : Übertritt Team 1
- 'I' : Übertritt Team 2
- 'B' : Ball im Aus
- 'R' : Fortsetzung

# Reaktionen
- Tor: an Startposition zurück, Ball geht an Verliererteam
- Deadlock: an Startposition zurück
- Übertritt: an Startposition zurück, Team, was nicht übertreten hat, kriegt den Ball
- Aus: "Einwurf" per Hand, EV3s warten, bis Ball wieder im Spiel ist
- An Startposition zurück: Fortsetzungssignal, wenn beide EV3s wieder an Startposition
